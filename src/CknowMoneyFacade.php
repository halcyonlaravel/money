<?php

namespace HalcyonLaravelBoilerplate\Money;

use Money\Money;
use Symfony\Component\HttpFoundation\Response;

/**
 * for compatibility purpose
 *
 */
final class CknowMoneyFacade implements MoneyContract
{
    public static function makeMoney($amount, $currency = null): Money
    {
        return money($amount, $currency)->getMoney();
//        return makeMoney($amount, $currency);
    }

    public static function makeBitcoin($amount): Money
    {
        abort(Response::HTTP_NOT_IMPLEMENTED, Response::$statusTexts[Response::HTTP_NOT_IMPLEMENTED]);
//        return makeBitcoin($amount);
    }

    /**
     * 100 Money -> '₱1.00'
     *
     * @param  \Money\Money  $money
     * @param  string|null  $locale
     *
     * @return string
     */
    public static function formatMoneyAsCurrency(Money $money, string $locale = null): string
    {
        if ($locale != null) {
            trigger_error('$locale is deprecated and will be remove in future release', E_USER_NOTICE);
        }

        if (is_string($symbol = config('halcyon-money.currency_symbol'))) {
            return $symbol.number_format(self::formatMoneyAsDecimal($money), 2);
        }

        return money($money->getAmount(), $money->getCurrency()->getCode())->format();

//        if (is_string($symbol = config('money.currency_symbol'))) {
//            return $symbol.number_format(self::formatMoneyAsDecimal($money), 2);
//        }
//        return formatMoneyAsCurrency($money, $locale);
    }

    /**
     * 100 Money to '1.00'
     *
     * @param  \Money\Money  $money
     *
     * @return string
     */
    public static function formatMoneyAsDecimal(Money $money): string
    {
        return money($money->getAmount(), $money->getCurrency()->getCode())->formatByDecimal();
//        return formatMoneyAsDecimal($money);
    }

    /**
     * 1 -> 100 Money
     *
     * @param  string  $amount
     * @param  string|null  $currency
     *
     * @return \Money\Money
     */
    public static function parseMoneyDecimal(string $amount, string $currency = null): Money
    {
        return money_parse_by_decimal($amount, $currency ?? config('money.defaultCurrency'))->getMoney();
    }

    public static function transform(Money $money): array
    {
        return array_merge(
            $money->jsonSerialize(),
            [
                'formatted' => self::formatMoneyAsCurrency($money),
            ]
        );
    }

    public static function resolve($money): Money
    {
        if (! $money instanceof Money) {
            $money = self::makeMoney($money['amount'], $money['currency'] ?? null);
        }

        return $money;
    }
}
