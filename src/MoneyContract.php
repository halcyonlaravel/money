<?php

namespace HalcyonLaravelBoilerplate\Money;

use Money\Money;

interface MoneyContract
{

    public static function makeMoney($amount, $currency = null): Money;

    public static function makeBitcoin($amount): Money;

    /**
     * 100 Money -> '₱1.00'
     *
     * @param  \Money\Money  $money
     * @param  string|null  $locale
     *
     * @return string
     */
    public static function formatMoneyAsCurrency(Money $money, string $locale = null): string;

    /**
     * 100 Money to '1.00'
     *
     * @param  \Money\Money  $money
     *
     * @return string
     */
    public static function formatMoneyAsDecimal(Money $money): string;

    /**
     * 1 -> 100 Money
     *
     * @param  string  $amount
     * @param  string|null  $currency
     *
     * @return \Money\Money
     */
    public static function parseMoneyDecimal(string $amount, string $currency = null): Money;

    public static function transform(Money $money): array;

    public static function resolve($money): Money;
}
