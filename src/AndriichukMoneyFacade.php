<?php

namespace HalcyonLaravelBoilerplate\Money;

use Money\Money;

/**
 * for compatibility purpose
 *
 */
final class AndriichukMoneyFacade implements MoneyContract
{
    public static function makeMoney($amount, $currency = null): Money
    {
        return makeMoney($amount, $currency);
    }

    public static function makeBitcoin($amount): Money
    {
        return makeBitcoin($amount);
    }

    /**
     * 100 Money -> '₱1.00'
     *
     * @param  \Money\Money  $money
     * @param  string|null  $locale
     *
     * @return string
     */
    public static function formatMoneyAsCurrency(Money $money, string $locale = null): string
    {
        if (is_string($symbol = config('halcyon-money.currency_symbol'))) {
            return $symbol.number_format(self::formatMoneyAsDecimal($money), 2);
        }
        return formatMoneyAsCurrency($money, $locale);
    }

    /**
     * 100 Money to '1.00'
     *
     * @param  \Money\Money  $money
     *
     * @return string
     */
    public static function formatMoneyAsDecimal(Money $money): string
    {
        return formatMoneyAsDecimal($money);
    }

    /**
     * 1 -> 100 Money
     *
     * @param  string  $amount
     * @param  string|null  $currency
     *
     * @return \Money\Money
     */
    public static function parseMoneyDecimal(string $amount, string $currency = null): Money
    {
        return parseMoneyDecimal($amount, $currency);
    }

    public static function transform(Money $money): array
    {
        return array_merge(
            $money->jsonSerialize(),
            [
                'formatted' => self::formatMoneyAsCurrency($money),
            ]
        );
    }

    public static function resolve($money): Money
    {
        if (! $money instanceof Money) {
            $money = self::makeMoney($money['amount'], $money['currency'] ?? null);
        }

        return $money;
    }
}
