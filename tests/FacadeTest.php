<?php

namespace HalcyonLaravelBoilerplate\Money\Tests;

use HalcyonLaravelBoilerplate\Money\CknowMoneyFacade as MoneyFacade;
use Money\Money;

class FacadeTest extends TestCase
{
    /**
     * @test
     */
    public function success()
    {
//        config(['money.defaultCurrency' => 'PHP']);

        $this->assertMoney(1, MoneyFacade::makeMoney(100, 'PHP'));
        $this->assertEquals('₱1.00', MoneyFacade::formatMoneyAsCurrency(Money::PHP(100)));
        $this->assertEquals('1.00', MoneyFacade::formatMoneyAsDecimal(Money::PHP(100)));
        $this->assertEquals(100, MoneyFacade::parseMoneyDecimal(1)->getAmount());

        $this->assertSame(
            [
                'amount' => '100',
                'currency' => 'PHP',
                'formatted' => '₱1.00',
            ],
            MoneyFacade::transform(MoneyFacade::makeMoney(100, 'PHP'))
        );

        $this->assertTrue(Money::PHP(100)->equals(MoneyFacade::resolve(['amount' => 100, 'currency' => 'PHP'])));
    }

    private function assertMoney(float $expected, Money $actual)
    {
        $this->assertTrue(Money::PHP($expected * 100)->equals($actual));
    }
}
