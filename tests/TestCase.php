<?php

namespace HalcyonLaravelBoilerplate\Money\Tests;

use HalcyonLaravelBoilerplate\Money\MoneyServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
//    public function setUp(): void
//    {
//        parent::setUp();
//
//        Factory::guessFactoryNamesUsing(
//            fn(string $modelName) => 'Spatie\\Skeleton\\Database\\Factories\\'.class_basename($modelName).'Factory'
//        );
//    }

    protected function getPackageProviders($app)
    {
        return [
            MoneyServiceProvider::class,
            \Cknow\Money\MoneyServiceProvider::class,
        ];
    }

//    public function getEnvironmentSetUp($app)
//    {
//        config()->set('database.default', 'testing');
//        /*
//        include_once __DIR__.'/../database/migrations/create_skeleton_table.php.stub';
//        (new \CreatePackageTable())->up();
//        */
//    }
}
