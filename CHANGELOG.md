# Changelog

All notable changes to `money` will be documented in this file.

## 2.0.0 - 2021-04-23

- Use cknow/laravel-model

## 1.1.1 - 2021-04-23

- Add interface

## 1.1.0 - 2021-04-23

- Rename config conflict with package config, add docs block on facade functions

## 1.0.0 - 2021-04-23

- initial release
